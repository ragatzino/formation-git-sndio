# Résumé formation git

## Git 

Intêret : travail mutualisé, versionné, retourner en arrière (sécurisé)

Fonctionne avec un répertoire propre le **.git**

## Travail en local

### Résumé

- Créer un dépot : **git init**
- Récupérer un dépot existant : **git clone <\url-du-depot>**
- Ajout de fichier : **git add .**
- Création d'une version/commmit : **git commit -m "message"**
- Création d'une branche : **git checkout -b nom-branche**
- Retour vers le passé / vers une branche : **git checkout <\sha-commit>** | **git checkout <\branche>**

<details><summary>Détails</summary>
<p>


### Initialiser un dépot
Créer un dépot Git vide
```
git init
```

Partir d'un dépot existant 
```
git clone <url-copiée-sur-gitlab.insee.fr>
```

### Ajout de fichiers

Ajouter tous les fichiers pour les préparer a la prochaine version:
```
git add . 
```

Ajouter un fichier pour la prochaine version :
```
git add fonction.R
```

Créer une nouvelle version : 
```
git commit -m "nouvelle-version"
```

### Navigation

Les branches sont 
Créer une nouvelle branche a partir de la branche où l'on se trouve

```
git checkout -b "nouvelle-branche"
```

Naviguer dans le passé : 
A partir de l'identifiant du commit récupéré via : 
- gitlab > history > accéder au commit puis copier l'identifiant

ou

```
git log
```

Puis
```
git checkout <identifiant-commit>
```


</p>
</details>

## Travail distant

### Résumé
- Liaison a un dépot distant : **git remote add** </url-depot> (ajouter/supprimer)
- Voir tous les dépots rattachés au dépot actuel : **git remote --verbose**
- Récupérer les données d'un dépot dans sa copie de travail : **git pull**
- Envoyer les versions (commits) sur le dépot distant : **git push**


<details><summary>Détails</summary>
<p>


### Liaison a un dépot distant

Vérifier la liaison 
```
git remote --verbose
```

> retourne les dépots distants connus par votre dépot local

Partir d'un dépot existant 
```
git clone <url-copiée-sur-gitlab.insee.fr>
```

Ajouter un nouveau dépot distant pour votre dépot local
```
git remote add <nom-du-depot> <url-du-depot>
```

> nom du dépot doit être origin pour être référencé par défaut pour git

### Pull

Permet de récupérer les informations d'un dépot distant

```
git pull <depot> <branche>
```

Par défaut : 

```
git pull
```

équivaut à :

```
git pull origin <branche-actuelle>
```

### Push

Envoyer ses changements au dépot distant, 

:warning: De préférence après récupération des données du dépot 

```
git push <depot> <branche>
```

Par défaut : 

```
git push
```

équivaut à :

```
git push origin <branche-actuelle>
```

</p>
</details>

