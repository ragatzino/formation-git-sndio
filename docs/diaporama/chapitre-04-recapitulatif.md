<!-- .slide: data-background-image="images/delo.png" data-background-size="600px" class="chapter" -->

## Récapitulatif des épisodes précédents

%%%

<!-- .slide: data-background-image="images/delo.png" data-background-size="600px" class="slide" -->

## Travailler en local (1/2)

<img src="./images/lifecycle.png" style="background-color:white" >
%%%

<!-- .slide: data-background-image="images/delo.png" data-background-size="600px" class="slide" -->

## Travailler en local (2/2)

<img src="./images/medium-reflog.png" style="background-color:white" >
%%%

<!-- .slide: data-background-image="images/delo.png" data-background-size="600px" class="slide" -->

## Travailler a distance

<img src="./images/ez-pull-push.png" style="background-color:white" >
%%%

<!-- .slide: data-background-image="images/delo.png" data-background-size="600px" class="slide" -->

## Schéma global
<img src="./images/pull-fetch.png" width="90%">