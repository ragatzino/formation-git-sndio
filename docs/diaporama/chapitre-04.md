<!-- .slide: data-background-image="images/rocket.svg" data-background-size="600px" class="chapter" -->

## Travailler avec un dépot distant

%%%

<!-- .slide: data-background-image="images/rocket.svg" data-background-size="600px" class="slide" -->

### Les avantages

Votre travail n'est pas stocké que à un endroit mais à plusieurs.
- vous pouvez perdre/casser votre poste ;)
- vous pouvez travailler depuis un autre poste

Et surtout, vous pouvez travailler à plusieurs !

%%%

<!-- .slide: data-background-image="images/rocket.svg" data-background-size="600px" class="slide" -->

### Les inconvénients

Deux commandes en plus : pull, push
%%%

<!-- .slide: data-background-image="images/rocket.svg" data-background-size="600px" class="slide" -->

### Zoom sur Pull

Je mets à jour ma branche local *main* avec les modifications que mes collègues ont mis sur le dépot distant :
<a href="https://git-scm.com/docs/git-pull">

~~~~
git pull origin main
~~~~

</a>

%%%

<!-- .slide: data-background-image="images/rocket.svg" data-background-size="600px" class="slide" -->


### Zoom sur Push

Je mets à jour la branche distante *main* avec les modifications après avoir mis a jour ma branche locale et ajouté mes changements
<a href="https://git-scm.com/docs/git-push">

~~~~
git push 
git push origin main
~~~~

</a>


%%%

<!-- .slide: data-background-image="images/rocket.svg" data-background-size="600px" class="slide" -->

### Exercices

<a href="exercices/tp-distant.html" target="_blank">
C'est ici
</a>



%%%

<!-- .slide: data-background-image="images/rocket.svg" data-background-size="600px" class="slide" -->


### Bonus : Récupérer les changements sans les intégrer

Prudemment, je mets à jours ma branche locale *origin/main*

Je l'inspecte avant de la merger dans ma branche locale *main*
<a href="https://git-scm.com/docs/git-fetch">

~~~~
git fetch
git merge
~~~~

</a>

