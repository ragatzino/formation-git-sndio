<!-- .slide: data-background-image="images/submarine.svg" data-background-size="600px" class="chapter" -->

## Travailler en local bonus

%%%
<!-- .slide: data-background-image="images/submarine.svg" data-background-size="600px" class="slide" -->

### Bonus 

Une partie bonus de ce qui n'est pas nécessaire pour développer directement, mais qui peut s'avérer pratique dans des situations, des notes sont disponibles dans la version speaker view, cela ne sera pas abordé.


%%%
<!-- .slide: data-background-image="images/submarine.svg" data-background-size="600px" class="slide" -->

### Remisage / Stash
- De manière native, git offre un système de nettoyage des fichiers que vous ne voulez pas ajouter à un commit, c'est le stash
```
git stash
```
- Cela permet donc par exemple de sauvegarder un travail encore temporaire dans une zone dédiée pour pouvoir l'appliquer plus tard (dans le cas d'un hotfix par exemple)
```
# regarder tous les index stashés
git stash list
# ajouter les fichiers stashés dans l'index
git stash apply
# ajouter les fichiers stashés dans l'index suppression de leur sauvegarde dans le stash
git stash pop
```



%%%
<!-- .slide: data-background-image="images/submarine.svg" data-background-size="600px" class="slide" -->

### Retourner dans le passé

- Vous pouvez en effet retourner dans le passé, si un changement actuel ne vous convient plus, il vous suffit de vous déplacer dans un version antérieure du code
```
git log --one-line
git checkout [sha]
```
- Vous pouvez également le faire fichier par fichier, en l'ajoutant au staging
```
git checkout [sha] fichier
```
- Vous pouvez enfin opter pour l'option proposée par reset : 
```
git reset --hard 
# retourner plus bas (3 commits en arrière ici)
git reset --hard HEAD^^^
```
Note:

%%%
<!-- .slide: data-background-image="images/submarine.svg" data-background-size="600px" class="slide" -->

### Versionner son appli proprement
- Ajouter les changements les plus récents au dernier commit 
```
git commit --amend
```
- Réécrire l'histoire locale 
```
git rebase --interactive HEAD~N
git rebase --interactive [sha]
```
- Mettre en place des jalons pour votre application : les tags.
```
git tag 
```
Note:
Lorsque vous travaillez en local, vous allez vouloir commiter souvent, pour pouvoir éventuellement pouvoir bouger sans soucis. Mais le problème est que lorsque qu'on commit, il faut se rappeler que c'est pour créer un historique cohérent à la fin. Donc il ne faut pas non plus polluer le dépot

Les tags sont littéralement des jalons pour votre application, ils permettent d'indexer des versions plus facilement afin de pouvoir donc éventuellement y revenir, ou de réaliser un versionning plus propre (v1 v2 ...)

%%%

<!-- .slide: data-background-image="images/submarine.svg" data-background-size="600px" class="slide" -->

### Exercices

[C'est ici](./exercices/bonus-exercice-git-local.html)