# Création d'un projet Git via Eclipse

## Ouvrir les outils Git

Onglet Window, Show View, Git Repositories

## Création d'un nouveau projet

Cliquez sur Create a new local Git repository. <br>
Entrez le nom de votre projet et son adresse. Par exemple : C:\Users\HGHJN4\outils\eclipse_workspace\toto

Vous venez de créer un dossier contenant un dossier caché .git. <br>
<img src="./images/dossier-git.png" width="10%"> <br>
Ce dossier caché représente votre *dépot local*. Il contient tous l'historique et vous permet de travailler localement même sans connexion à internet.

Reste maintenant à importer le projet. <br>
Dans la vue "Git Repositories", clique droit sur le dépot local, \*Import Projects..." <br>
Finish.<br>

Il ne reste plus qu'à coder ! Et à relier votre dépot local à un dépot distant.


