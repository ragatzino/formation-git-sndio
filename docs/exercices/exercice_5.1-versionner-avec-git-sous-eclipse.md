# Versionner son code avec Git et l'IDE Eclipse

Vous pouvez toujours passer par le terminal pour versionner en ligne de commande.
Onglet Window, Show View, Terminal

Pour ceux qui ne veulent pas des lignes de commandes passez par la vue Git Staging (Onglet Window, Show View, Git Staging)

<img src="./docs/images/gitStagingEclipse.PNG" width="10%">

Unstaged Changed : les fichiers modifiés que vous pouvez adder <br>
Cliquez sur + pour adder les fichiers sélectionnés.
En ligne de code vous feriez :

```
git add nomFichier1 nomFichier2
```

Cliquez sur ++ pour adder tous les fichiers modifiés. <br>
En ligne de code vous feriez :

```
git add .
```

Staged Changed : les fichiers addés que vous pouvez commiter. <br>
N'oubliez pas le message de commit puis cliquez sur Commit. <br>
On évite le _Commit and Push_ qui fera histoire moche sans profiter de la décentralisation...<br>
En ligne de code vous feriez :

```
git commit -m "message utile"
```
