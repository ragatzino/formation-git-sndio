# Création d'un projet Git

## Création d'un nouveau projet

Placez vous là où vous voulez créer votre projet. Un clic droit dans le dossier, Git Bash Here. <br>
Initialisez votre projet
~~~
git init nom-projet
~~~
Vous venez de créer un dossier contenant un dossier caché .git. <br>
<img src="./docs/images/dossier-git.png" width="250"> <br>
Ce dossier caché représente votre "dépot local". Il contient tout l'historique et vous permet de travailler localement même sans connexion à internet.

Il ne reste plus qu'à coder ! Et à relier votre dépot local à un dépot distant.

## Modification du projet

Créer un fichier texte (.txt ou .md) <br>
Pour l'instant ce fichier n'existe que sur votre ordinateur. Votre dépot local ne le connais pas encore.

Rappelez vous ce schéma :

<img src="./images/schemaDepot.JPG" width="10%"> <br>

Les fichiers vont connaitre trois stades :
* untracked
* staged (ou added)
* unstaged (ou modified)

<img src="./images/added-modified.JPG" width="10%"> <br>

Il faut donc maintenant rendre le fichier "connu"/*tracked* de Git.

Dans git bash :
~~~~
git add adresse-et-nom-du-fichier
~~~~
Par exemple :
~~~~
git add toto.md
git add dossier/sous-dossier/toto.md
~~~~
Ou si vous voulez ajouter les modifications faites sur plusieurs fichiers :
~~~~
git add .
~~~~
Prendra en compte toutes les modifications des fichiers présents dans le dossier où vous vous trouvez (représenté par le .).

Votre fichier est désormais *staged* (ou *added* ou *tracked*...). Mais il ne fait pas encore partie de "l'histoire" (ni locale, ni distante).<br>
Faisons le rentrer dans l'histoire :

~~~~
git commit -m "un message explicite"
~~~~

Les modifications que vous avez prises en compte grâce au *git add* sont maintenant rassemblées dans un paquet (un *commit*) qui fait partie de l'histoire locale. <br>
N'oubliez pas d'ajouter un message ( -m ) à ce *commit* (de toute façon c'est obligatoire).

Vous voulez maintenant que le dépot distant sache que l'histoire a changé.

Attention, ici on vient d'initialiser un nouveau projet, donc pas de risque de conflits. S'il y a le moindre risque de conflit, il faut utiliser les commandes *git status* et *git pull* (qu'on verra ensuite).

~~~~
git push
~~~~

Toutes les modifications de l'histoire (les *commit*) que vous avez inscrites en local sont maintenant aussi inscrites dans le dépot distant.

Mais oups on n'a pas créé de dépot distant ^^
~~~~
fatal: No configured push destination.
Either specify the URL from the command-line or configure a remote repository using

    git remote add <name> <url>

and then push using the remote name

    git push <name>
~~~~

Y'a plus qu'à faire ce qu'il demande :

~~~~
git remote add <name> <url>
git remote add origin [git@git.stable.innovation.insee.eu:22222]:<votre idep>/<le-nom-du-projet>.git
~~~~

On ajoute un dépot distant. <br>
Il peut y en avoir plusieurs donc il faut lui donner un nom. Vous pouvez appeler votre dépot distant *toto* mais par convention on préfère appeler *origin* le dépot distant dont l'histoire fera foi.

Tentez un git push
~~~~
$ git push
fatal: The current branch branche has no upstream master.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin master
~~~~

Git ne sait pas où pusher, ni quoi pusher.<br>
Deux solutions : 
* soit vous lui indiquez à chaque fois où il faut pusher (sur votre dépot distant nommé *origin*) et ce qu'il faut pusher (en général votre branche main). Fatiguant.
~~~~
git push origin main
~~~~
* soit vous lui indiquez une bonne fois pour toute que votre branche main devra être pushée sur la branche main de votre dépot distant nommé *origin*
~~~~
git push --set-upstream origin main
~~~~
Dorénavant vous vous contenterez d'un *git push* pour pusher sur le dépot distant *origin* votre branche main.

## Que versionner ?

Seul le code doit être versionné ! <br>
Éviter les faux fichiers texte (.odt). <br>
Les data n'ont pas à être versionnées (on pourra en reparler) <br>
Pour ne versionner que ce que vous voulez : indiquez dans un fichier ce que vous ne voulez pas versionner. <br>
Ce ficher se nomme .gitignore, le plus simple est de le mettre à la racine.
[Un exemple du .gitignore](https://git.stable.innovation.insee.eu/outils-transverses/migration-svn-git/-/tree/master#cr%C3%A9er-un-fichier-gitignore) extrait d'une bonne doc déjà citée.




