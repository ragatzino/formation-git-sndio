# Git en local vscode

Cet exercice couvre les notions principales pour travailler en local avec git.
Le principe est la construction d'un petit projet pour ensuite le déployer sur un dépot distant. 
## Initialisation d'un projet

Pour cela vous allez créer un dossier dans un emplacement de votre choix (exemple dans ~/), vous êtes libre sur le choix du nom et d'y ajouter des fichiers.

Ensuite initiez un projet git prenant tous les éventuels fichiers de ce dossier.

<details><summary>Solution</summary>
<p>

```
git init
```

Ou si vous y avez mis des fichiers
```
git init .
```

</p>
</details>
<br/>
Vous pouvez désormais constater qu'il se trouve être un dossier .git (dossier caché) à votre position dans l'arborescence.

> Ce fichier contient toute l'information sur la vie du dépot et il permet l'utilisation de git pour votre projet.

## Ajout d'un fichier 

Toujours dans ce dossier créez un fichier index.html contenant les informations suivantes : 

Formation git orléans 29/10/2020 dans une balise div.

Puis ajoutez le fichier index.html à l'index et créez un commit, permettant le versionning de l'application. (exemple pour le message de commit : "ajout du fichier index.html")

<details><summary>Solution</summary>
<p>

```
echo "<div>Formation git orléans 29/10/2020</div>" > index.html    
git add index.html
git commit -m "ajout du fichier index.html"
```

</p>
</details>
<br/>

Vérifiez ensuite que vous l'avez bien commité 
<details><summary>Solution</summary>
<p>

```
git log
```
ou 
```
git log --oneline
```

</p>
</details>
<br/>

## Création d'une branche 
A partir de ce commit, créez une branche que vous appelerez demo-site. Déplacez vous sur cette branche 
<details><summary>Solution</summary>
<p>

```
git branch demo-site
git checkout demo-site
```

</p>
</details>
<br/>

Ensuite importez les feuilles de styles suivantes et ajoutez les en commitant


<a href="../css/modest.css" download>
    ici : 
  <img src="https://upload.wikimedia.org/wikipedia/commons/d/d5/CSS3_logo_and_wordmark.svg" width=100>
</a>