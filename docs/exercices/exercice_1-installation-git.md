# Installation de Git et IDE(s)

## :black_nib: Téléchargez un IDE : VSCode ou Eclipse

### Télécharger VSCode

[Sur leur site](https://code.visualstudio.com/)

<details><summary>Détails installer</summary>
<p>

Download for Windows <br>
Enregistrez le fichier <br>
Double cliquez sur le .exe <br>

Une boite de dialogue s'ouvre :<br>
Acceptez les termes du contrat de licence <br>
Suivant <br>
Choisissez le lieu d'installation (par exemple C:\Users\HGHJN4\outils) <br>
Suivant <br>
Installer <br>
Terminer <br>

par défaut mettre un raccourci VSCode sur le bureau

</p>
</details>

### Utiliser Eclipse

[Une doc d'installation par la DAAP](https://git.stable.innovation.insee.eu/animation-dev/outils-de-dev/-/wikis/Outils/Eclipse_Fiche) (Division Architecture applicative de production)

## :school_satchel: Installer Git 

[Une doc d'installation par la DAAP](https://git.stable.innovation.insee.eu/animation-dev/outils-de-dev/-/wikis/Outils/Git_Fiche) (Division Architecture applicative de production)

[Allez sur le site de Git](https://git-scm.com/)

<details><summary>Détails installer</summary>
<p>

Download x.x.x for Windows <br>
Enregistrez le fichier <br>
Double cliquez sur le .exe <br>

Une boite de dialogue s'ouvre :<br>
Next <br>
Choisissez le lieu d'installation (par exemple C:\Users\HGHJN4\outils) <br>
Next <br>

Si on vous propose le choix d'éditeur de texte, choisissez :
* VSCode : Use Visual Code Studio Code as Git's defaut editor <br>
* ou
* NotePad ++

</p>
</details>


Pour finir la configuration suivre ce bon [tuto](https://git.stable.innovation.insee.eu/outils-transverses/migration-svn-git/-/blob/master/annexes/config-bash.md) extrait [d'ici](https://git.stable.innovation.insee.eu/outils-transverses/migration-svn-git/-/tree/master)<br>

Ces commandes peuvent être passées avec VSCode, l'invite de commande de Windows, Git Bash...

```
git config --global user.name "Prénom Nom"
git config --global user.email "prenom.nom@insee.fr"
git config --global http.proxy proxy-rie.http.insee.fr:8080
```

Vérifiez que c'est bien pris en compte.

### Verifications 

```
git config user.name
```

Doit retourner : votre prénom puis votre nom
```
git config user.email
```

Doit retourner : votre email Insee
```
git config http.proxy
```
Doit retourner : http://proxy-rie.http.insee.fr:8080

Si vous voulez connaitre la configuration complète de git :

```
git config –-list
```

(tapez sur "Entrez" pour dérouler les "pages" et tapez "q" pour quitter)

### :closed_lock_with_key: Bonus: authentification
```
ssh-keygen -t ed25519 -C "gitlab.insee.fr"
```

<details><summary>détails</summary>
<p>

    
Le -C est une option facultative pour ajouter un commentaire à la clé. Cela peut permettre de différencier les paires de clés si on en a plusieurs. Ici le commentaire choisit est le mail. <br>
Le -t permet d'indiquer quel type de clé on veut. Il existe [différents algorithmes](https://docs.gitlab.com/ee/ssh/README.html#review-existing-ssh-keys) pour générer des clés. L'algorithme conseillé est le ed25519.

    
</p>
</details>
<br/>

Il est préférable de déposer vos clés dans **C:\Users\idep\.ssh** 
plutôt que **Z:\\.ssh**

> :warning: Nommez bien votre clé id_ed25519. Si la clé se nomme autrement que id_NomDeLAlgo votre client SSH ne la trouvera pas. 

### :unlock: Bonus : travail avec github/gitlab.com en https : 
Vous pouvez effectivement travailler en https mais il faut enlever une couche de sécurité, qui fait qu'on ne vérifie pas le certificat https..

```
git config http.sslVerify "false"
```
