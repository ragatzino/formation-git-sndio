# Cloner un projet

## Ouvrir les outils Git

Onglet Window, Show View, Git Repositories

## Cloner

Dans la vraie vie on initialise pas un nouveau projet tous les matins. On reparts souvent du code du prédécesseur ou d'un collègue.<br>
Comment faire ?

Trouver l'URL de votre projet.<br>
Assurez vous que vous avez des droits de modifications sur ce projet (il faut être _developer_, _maintainer_, _owner_). <br>
Vous voulez participer à un projet sur lequel vous n'avez pas de droit ? ça on en parlera plus tard.

Cliquez sur Clone a Git Repository. <br>
Entrez l'URL dans l'URI<br>
Next<br>
On vous présente les différentes branches de ce dépôt. <br>
Choisissez où vous voulez déposer votre dépot local.<br>
Finish.<br>

Vous venez de créer une copie exacte du dépot distant. Tout l'historique, toutes l'histoire du code écrite sur ce dépot est désormais dans un dépot local sur votre ordinateur.

Reste maintenant à importer le projet. <br>
Dans la vue "Git Repositories", clique droit sur le dépot local, \*Import Projects..." <br>
Finish.<br>

On peut maintenant coder.<br>
On revient à la perspective Java. Onglet Window, Perspective, Open Perspective, Java. Et c'est parti !